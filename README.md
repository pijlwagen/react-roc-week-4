# Week 4
## Design realiseren
Goedemorgen avonturiers,

De taak van deze week is om het design in deze repository na te maken (zie design.png). Het design is gemaakt in Bootstrap met React. Ik verwacht dat onderdelen die hergebruikt worden (denk aan: navbar, card, container, row, columns e.d.) een eigen component krijgen.

Als je geen idee hebt hoe je dit aan pakt, is een goed begin om een relatieboom te maken. Kom je er niet uit, of is er iets onduidelijk? Vraag tijdig om hulp (op locatie, of in #vragen op Discord)!

## App maken
Sinterklaas vond het tijd voor een geintje, <br>
Want als front-end student ben je geen kleinigheidje. <br>
<br>
In React ben je vast een echte held, <br>
Maar voor wie 't nog niet weet, wordt hier iets verteld. <br>
<br>
Bootstrap installeren, een fluitje van een cent,<br>
Een paar commando's en je bent present.<br>
<br>
Dus duik in de code, met plezier en pracht,<br>
En maak die app, zoals het hoort, volbracht.

```bash
npm create vite@latest design-realiseren-week-4
cd design-realiseren-week-4
npm i bootstrap sass
```

Gefeliciteerd, U heeft nu een React app gemaakt en Bootstrap geinstalleerd. De enige stap die over blijft is het gebruik maken van Bootstrap.

Wat U hiervoor doet is als volgend:
1. Rename `src/index.css` naar `src/index.scss`.
2. Verwijder alles in `src/index.scss`.
3. Importeer bootstrap in `src/index.scss` met `@import "../node_modules/bootstrap/scss/bootstrap.scss";`

Dankzij SASS kunnen wij de vormgeving van Bootstrap gemakkelijk aanpassen. Elk component in Bootstrap heeft namelijk eigen SASS variables, als voorbeeld pakken we even de [Card](https://getbootstrap.com/docs/5.3/components/card/) er bij. Onderaan deze pagina vind je een lijst met SASS variabele voor dit component, bijvoorbeeld `$card-border-width`; Als je deze variabele aanpast __voordat__ je Bootstrap importeert, dan gebruikt Bootstrap jouw waarde als waarde voor de variabele en zal de card dus geen border meer hebben. Een ander voorbeeld zou zijn de `$card-bg`, hiermee kun je dus de background color van de card aanpassen. Zo kun je dus makkelijk de vormgeving van Bootstrap aanpassen.

In het design zul je zien dat er gebruik word gemaakt van icoontjes. Dit word gedaan door middel van de lijst met Material icons op [xicons](https://xicons.org). xIcons is een verzameling aan Icon libraries, dankzij xIcons kunnen wij gemakkelijk icoontjes gebruiken in onze app. 

Wij gaan in deze opdracht specifiek gebruik maken van Material icons (Google). Als eerste zullen wij de benodigde dependencies moeten installeren.

```
npm i @ricons/material @ricons/utils
```

Hierna kun je gebruik maken van icons in je react app door de `Icon` helper en de icon die je wilt te importeren, een voorbeeld vind je hier:


```jsx
import { PolylineOutlined } from '@ricons/material' // De icons die je wilt gebruiken.
import { Icon } from '@ricons/utils' // De helper component om Icons in te laden, zo kun je met de properties zoals size="" de grootte aanpassen.
import Card from './components/Card'

...
<Card>
    <Icon>
        <PolylineOutlined />
    </Icon>
</Card>
...
```

## Starten van de app
```npm run dev```